# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2021 Baptiste Beauplat <lyknode@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from pytest import mark


@mark.parametrize('route,redirect', (
    ('/package/', ''),
    ('/binary/', ''),
    ('/package/?src=plop', 'package/plop/'),
    ('/binary/?bin=plop', 'binary/plop/'),
))
def test_package_index_redirect(client, route, redirect):
    response = client.get(route)

    assert response.status_code == 302
    assert response.location == f'http://localhost/{redirect}'


@mark.parametrize('route,name,category', (
    ('package', 'source', 'l',),
    ('binary', 'binary', 'l',),
    ('package', 'source', 'not_found',),
    ('binary', 'binary', 'not_found',),
))
def test_package_index_cat_not_found(client, route, name, category):
    response = client.get(f'/{route}/?cat={category}')

    assert response.status_code == 404
    assert f'No {name} packages in this category' in response.data.decode()


@mark.parametrize('route,name', (
    ('package', 'source'),
    ('binary', 'binary'),
))
def test_package_index_cat(client, route, name, package_view, breadcrumbs):
    response = client.get(f'/{route}/?cat=z')

    crumbs = [
        {
            'url': '/',
            'name': 'localhost',
            'sep': '|',
        },
        {
            'name': f'{name} package:',
            'sep': '',
        },
        {
            'name': 'z*',
            'sep': '/',
        },
    ]

    assert response.status_code == 200
    assert breadcrumbs.exists(crumbs, response.data.decode())
    assert package_view.has_packages('z', response.data.decode())


@mark.parametrize('route,name', (
    ('package', 'source'),
    ('binary', 'binary'),
))
def test_package_versions_not_found(client, route, name):
    response = client.get(f'/{route}/not_found/')

    assert response.status_code == 404
    assert f'No such {name} package' in response.data.decode()


@mark.parametrize('route,name', (
    ('package', 'source'),
    ('binary', 'binary'),
))
def test_package_versions_invalid(client, route, name):
    response = client.get(f'/{route}/été/')

    assert response.status_code == 404
    assert 'No such package' in response.data.decode()


@mark.parametrize('route,name,package,category', (
    ('package', 'source', 'zsh', 'z'),
    ('binary', 'binary', 'zsh', 'z'),
    ('package', 'source', 'libstdc++6', 'libs'),
    ('binary', 'binary', 'libstdc++6', 'libs'),
))
def test_package_versions(client, route, name, package, category, package_view,
                          breadcrumbs):
    response = client.get(f'/{route}/{package}/')

    crumbs = [
        {
            'url': '/',
            'name': 'localhost',
            'sep': '|',
        },
        {
            'name': f'{name} package:',
            'sep': '',
        },
        {
            'url': f'/{route}/?cat={category}',
            'name': f'{category}*',
            'sep': '/',
        },
        {
            'name': f'{package}',
            'sep': '/',
        },
    ]

    assert response.status_code == 200
    assert breadcrumbs.exists(crumbs, response.data.decode())

    if name == 'binary':
        assert package_view.has_binary_versions(package, response.data.decode())
    else:
        assert package_view.has_source_versions(package, response.data.decode())


def test_package_version_not_found(client):
    response = client.get('/package/zsh/not_found/')

    assert response.status_code == 404
    assert 'No source or binary package' in response.data.decode()


@mark.parametrize('package,category,version', (
    ('zsh', 'z', '1.10'),
    ('libstdc++6', 'libs', '10.2.1'),
))
def test_package_version(client, package, category, version, package_view,
                         breadcrumbs):
    response = client.get(f'/package/{package}/{version}/')

    crumbs = [
        {
            'url': '/',
            'name': 'localhost',
            'sep': '|',
        },
        {
            'name': 'source package:',
            'sep': '',
        },
        {
            'url': f'/package/?cat={category}',
            'name': f'{category}*',
            'sep': '/',
        },
        {
            'url': f'/package/{package}/',
            'name': package,
            'sep': '/',
        },
        {
            'name': version,
            'sep': '/',
        },
    ]

    assert response.status_code == 200
    assert breadcrumbs.exists(crumbs, response.data.decode())
    assert package_view.has_package_version(package, version,
                                            response.data.decode())
