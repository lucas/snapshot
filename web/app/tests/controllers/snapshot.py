# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from shutil import rmtree
from os.path import join, dirname, abspath, isdir
from os import environ
from subprocess import Popen, STDOUT, PIPE
from tempfile import mkdtemp
from logging import getLogger

log = getLogger(__name__)


class SnapshotController():
    def __init__(self):
        self.db = None
        self.config = None
        self.farm = mkdtemp(prefix='snapshot-farm-')
        self.archives = []

    def cleanup(self):
        if isdir(self.farm):
            rmtree(self.farm)

    def snapshot(self, timestamp, archive):
        if archive.name not in self.archives:
            self._create_archive(archive.name)

        self._run_snapshot('import', '--path', archive.archive,
                           '--archive', archive.name, '--date', timestamp)
        self._run_snapshot('index')

    def _install_config(self):
        self.config = join(self.farm, 'config.yml')
        content = f"""db:
  connectstring: "{self.db}"
snapshot:
  farmpath: {self.farm}
"""

        with open(self.config, 'wb') as config:
            config.write(content.encode())

    def _run_snapshot(self, *args):
        if not self.config:
            self._install_config()

        cmd = (
            join(dirname(abspath(__file__)), '..', '..', '..', '..',
                 'snapshot'),
            '--config',
            self.config,
            '--log-to-stderr',
            '--verbose',
        )
        env = environ.copy()
        env['TZ'] = 'UTC'
        proc = Popen(cmd + args,
                     stdout=PIPE,
                     stderr=STDOUT,
                     env=env)
        (output, _) = proc.communicate()

        if proc.returncode != 0:
            raise Exception('command failed:\n'
                            f'command: {cmd}\n'
                            f'args: {args}\n'
                            f'output: {output.decode()}')

        return output.strip()

    def _create_archive(self, name):
        self._run_snapshot('add-archive', '--archive', name)
        self.archives.append(name)


class SnapshotDBController():
    def __init__(self, db):
        self.db = db

    def get_source_packages(self):
        self.db.execute(
            """SELECT DISTINCT
                   name
               FROM
                   srcpkg;""")

        return [row[0] for row in self.db.fetchall()]

    def get_source_packages_startswith(self, start):
        self.db.execute(
            """SELECT DISTINCT
                   name
               FROM
                   srcpkg
               WHERE name
                   LIKE %s;""", (f'{start}%',))

        return [row[0] for row in self.db.fetchall()]

    def get_source_versions(self, package):
        self.db.execute(
            """SELECT DISTINCT
                   version
               FROM
                   srcpkg
               WHERE
                   name = %s;""", (package,))

        return [row[0] for row in self.db.fetchall()]

    def get_binary_files(self, package, version):
        self.db.execute(
            """SELECT
                   hash,
                   architecture
               FROM
                   binpkg
                   LEFT JOIN file_binpkg_mapping USING (binpkg_id)
               WHERE
                   name = %s
                   AND version = %s;""", (package, version,))

        return [{'hash': row[0], 'architecture': row[1]}
                for row in self.db.fetchall()]

    def get_source_files(self, package, version):
        self.db.execute(
            """SELECT
                   hash
               FROM
                   srcpkg
                   LEFT JOIN file_srcpkg_mapping USING (srcpkg_id)
               WHERE
                   name = %s
                   AND version = %s;""", (package, version,))

        return [row[0] for row in self.db.fetchall()]

    def get_file_info(self, digest):
        files = []

        self.db.execute(
            """SELECT
                   file.name,
                   size,
                   run,
                   archive.name AS archive,
                   path
               FROM
                   file
                   LEFT JOIN node USING (node_id)
                   LEFT JOIN mirrorrun ON node.first = mirrorrun.mirrorrun_id
                   LEFT JOIN archive USING (archive_id)
                   LEFT JOIN directory ON node.parent = directory.directory_id
               WHERE
                   HASH = %s
               ORDER BY
                   run;""", (digest,)
        )

        for row in self.db.fetchall():
            files.append({
                'name': row[0],
                'size': row[1],
                'first_seen': row[2].strftime('%Y%m%dT%H%M%SZ'),
                'archive_name': row[3],
                'path': row[4],
            })

        return files

    def get_binary_packages(self, source, version):
        packages = []

        self.db.execute(
            """SELECT
                   binpkg.name,
                   binpkg.version
               FROM
                   binpkg
                   LEFT JOIN srcpkg USING (srcpkg_id)
               WHERE
                   srcpkg.name = %s
                   AND srcpkg.version = %s
               ORDER BY
                   name,
                   version""", (source, version,)
        )

        for row in self.db.fetchall():
            packages.append({
                'name': row[0],
                'version': row[1],
            })

        return packages

    def get_binary_versions(self, source):
        versions = []

        self.db.execute(
            """SELECT
                   binpkg.name,
                   binpkg.version as binary_version,
                   srcpkg.name as source,
                   srcpkg.version
               FROM
                   binpkg
                   LEFT JOIN srcpkg USING (srcpkg_id)
               WHERE
                   srcpkg.name = %s
               ORDER BY
                   name,
                   version""", (source,)
        )

        for row in self.db.fetchall():
            versions.append({
                'name': row[0],
                'binary_version': row[1],
                'source': row[2],
                'version': row[3],
            })

        return versions

    def get_dates(self, archive):
        dates = {}

        self.db.execute(
            """SELECT DISTINCT
                   SUBSTRING(DATE(run)::text, 1, 7)
               FROM
                   mirrorrun
               WHERE
                   archive_id = (
                       SELECT
                           archive_id
                       FROM
                           ARCHIVE
                       WHERE
                           name = %s)
               ORDER BY
                   1;""", (archive,))
        result = self.db.fetchall()

        for date in result:
            date = date[0]
            year = int(date.split('-')[0])
            month = int(date.split('-')[1])

            if year in dates:
                dates[year][month] = None
            else:
                dates[year] = {month: None}

        return [
            {'year': year, 'months': list(months.keys())}
            for year, months in dates.items()
        ]

    def get_runs(self, archive, year, month):
        self.db.execute(
            """SELECT
                   run
               FROM
                   mirrorrun
               WHERE
                   archive_id = (
                       SELECT
                           archive_id
                       FROM
                           ARCHIVE
                       WHERE
                           name = %s)
                   AND SUBSTRING(DATE(run)::text, 1, 7) = %s
               ORDER BY
                   1""", (archive, f'{year}-{int(month):02}')
        )

        return [
            run[0] for run in self.db.fetchall()
        ]

    def get_files(self, archive, date, path):
        files = []

        self.db.execute(
            """SELECT
                   first.run,
                   regexp_replace(coalesce(d.path, ''), '.*/', '') ||
                       coalesce(f.name, '') || coalesce(s.name, ''),
                   f.size,
                   s.target
               FROM
                   node
                   RIGHT JOIN directory ON (directory_id = parent
                           AND path = %s)
                   JOIN mirrorrun AS FIRST ON (node.first = first.mirrorrun_id)
                   JOIN mirrorrun AS LAST ON (node.last = last.mirrorrun_id)
                   RIGHT JOIN archive ON (archive.archive_id = first.archive_id
                           AND archive.name = %s)
                   LEFT JOIN directory AS d ON (node.node_id = d.node_id)
                   LEFT JOIN file AS f ON (node.node_id = f.node_id)
                   LEFT JOIN symlink AS s ON (node.node_id = s.node_id)
               WHERE
                   last.run >= %s
                   AND first.run <= %s
                   AND (d.path <> %s OR d.path IS NULL)
               ORDER BY
                   2;""", (path, archive, date, date, path))

        for row in self.db.fetchall():
            info = {
                'last': row[0],
                'name': row[1],
                'size': row[2],
                'dest': row[3],
            }

            if info['size']:
                info['type'] = 'file'

            elif info['dest']:
                info['type'] = 'symlink'

            else:
                info['type'] = 'directory'

            files.append(info)

        return files

    def get_first_last(self, archive, path):
        self.db.execute(
            """SELECT
                   first.run,
                   last.run
               FROM
                   directory
                   LEFT JOIN node USING (node_id)
                   LEFT JOIN mirrorrun AS FIRST ON first.mirrorrun_id = FIRST
                   LEFT JOIN mirrorrun AS LAST ON last.mirrorrun_id = LAST
                   LEFT JOIN archive ON first.archive_id = archive.archive_id
               WHERE
                   path = %s
                   AND archive.name = %s;
            """, (path, archive,)
        )

        return self.db.fetchone()

    def get_prev_next(self, archive, run):
        runs = {
            'prev': None,
            'next': None,
        }

        for key, operator, order in (
            ('prev', '<', 'DESC'),
            ('next', '>', 'ASC'),
        ):
            self.db.execute(
                f"""SELECT
                        run
                    FROM
                        mirrorrun
                        LEFT JOIN archive USING (archive_id)
                    WHERE
                        archive.name = %s
                        AND run {operator} %s
                    ORDER BY
                        run {order}
                    LIMIT 1;
                """, (archive, run,)
            )

            result = self.db.fetchone()

            if result:
                runs[key] = result[0]

        return runs.values()

    def get_mirrorrun_at(self, archive, date):
        self.db.execute(
            """SELECT
                   run,
                   mirrorrun_id,
                   archive_id
               FROM
                   mirrorrun
                   LEFT JOIN archive USING (archive_id)
               WHERE
                   name = %s
                   AND run <= %s
               ORDER BY
                   run DESC
               LIMIT 1;
            """, (archive, date)
        )

        result = self.db.fetchone()

        return {
            'run': result[0],
            'mirrorrun_id': result[1],
            'archive_id': result[2],
        }

    def get_stat(self, run, path):
        self.db.execute("SELECT * FROM stat(%s, %s);", (path, run,))
        result = self.db.fetchone()

        return {
            'filetype': result[0],
            'path': result[1],
            'directory_id': result[2],
            'node_id': result[3],
            'digest': result[4],
            'size': result[5],
        }

    def get_children(self, directory):
        self.db.execute("SELECT * FROM node_with_ts2 WHERE parent = %s",
                        (directory,))
        results = self.db.fetchall()

        return [{
            'node_id': result[0],
            'parent': result[1],
            'first': result[2],
            'last': result[3],
            'first_run': result[4],
            'last_run': result[5],
            } for result in results
        ]

    def get_neighbors(self, run):
        self.db.execute(
            """SELECT
                   *
               FROM (
                   SELECT
                       mirrorrun_id,
                       run,
                       LEAD(run, 1) OVER (ORDER BY run) AS next,
                       LAG(run, 1) OVER (ORDER BY run) AS prev
                   FROM
                       mirrorrun
                   WHERE
                       archive_id = (
                           SELECT
                               archive_id
                           FROM
                               mirrorrun
                           WHERE
                               mirrorrun_id = %s)) AS runs
               WHERE
                   mirrorrun_id = %s;
            """, (run, run,)
        )

        result = self.db.fetchone()

        return {
            'run': result[1],
            'next': result[2],
            'prev': result[3],
        }

    def get_neighbors_changes(self, archive, date, path):
        """
        The following code is a python implementation of the SQL request
        mirrorruns_get_neighbors_change.

        NEXT: first on first or next after last
        PREV: last on last or prev before first

        Or:
        When a file appears:
          - next == first
          - prev == prev before first
        When a file disappears:
          - next == next after last
          - prev == last
        """

        run = self.get_mirrorrun_at(archive, date)
        directory = self.get_stat(run['mirrorrun_id'], path)['directory_id']
        children = self.get_children(directory)

        post_first = [child['first_run'] for child in children if
                      child['first_run'] > run['run']]
        pre_first = [child['first_run'] for child in children if
                     child['first_run'] <= run['run']]
        post_last = [child['last_run'] for child in children if
                     child['last_run'] >= run['run']]
        pre_last = [child['last_run'] for child in children if
                    child['last_run'] < run['run']]

        pre_before_first = self.get_neighbors(
            self.get_mirrorrun_at(archive, max(pre_first))['mirrorrun_id']
        )['prev']
        post_after_last = self.get_neighbors(
            self.get_mirrorrun_at(archive, min(post_last))['mirrorrun_id']
        )['next']

        pre = None
        post = None

        if pre_before_first and pre_last:
            pre = max(pre_last, pre_before_first)
        elif not pre_last:
            pre = pre_before_first

        if post_after_last and post_first:
            post = min(post_first, post_after_last)
        elif not post_first:
            post = post_after_last

        return (pre, post)

    def get_last_mirrorrun(self):
        self.db.execute("SELECT max(run) FROM mirrorrun;",)
        row = self.db.fetchone()

        return row[0]
