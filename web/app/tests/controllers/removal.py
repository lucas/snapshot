# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from re import search
from logging import getLogger

log = getLogger(__name__)


class RemovalController():
    def __init__(self, db):
        self.db = db

    def add(self, id, timestamp, files, log):
        self.db.execute('INSERT INTO removal_log (removal_log_id, entry_added,'
                        'reason) VALUES (%s, %s, %s)', (id, timestamp, log,))
        for filename in files:
            hash = self._get_hash(filename)
            self.db.execute('INSERT INTO removal_affects '
                            '(removal_log_id, hash) VALUES '
                            '(%s, %s)', (id, hash,))

        self.db.execute('COMMIT')

    def _get_hash(self, filename):
        return self._get_fileinfo(filename)[0]

    def _get_fileinfo(self, filename):
        self.db.execute(
            """SELECT DISTINCT ON (file.name)
                 hash,
                 size,
                 run,
                 archive.name,
                 CASE WHEN binpkg.name IS NOT NULL THEN
                    binpkg.name
                 ELSE
                    srcpkg.name
                 END,
                 CASE WHEN binpkg.version IS NOT NULL THEN
                    binpkg.version
                 ELSE
                    srcpkg.version
                 END
                 FROM file
                   LEFT JOIN node
                     USING (node_id)
                   LEFT JOIN mirrorrun
                     ON last = mirrorrun_id
                   LEFT JOIN archive
                     USING (archive_id)
                   LEFT JOIN file_binpkg_mapping
                     USING (hash)
                   LEFT JOIN binpkg
                     USING (binpkg_id)
                   LEFT JOIN file_srcpkg_mapping
                     USING (hash)
                   LEFT JOIN srcpkg
                     ON file_srcpkg_mapping.srcpkg_id = srcpkg.srcpkg_id
                 WHERE file.name = %s
                 ORDER BY file.name, file_id DESC""", (filename,))
        result = self.db.fetchone()

        return result

    def exists_in_affected_files(self, filename, haystack):
        (hash, size, run, archive, source, version,) = \
            self._get_fileinfo(filename)
        archive_url = f'/archive/{archive}/'
        archive_url += f'{run.strftime("%Y%m%dT%H%M%SZ")}'
        pool_url = f'/pool/main/{source[0:1]}/{source}'

        needle = r'<dt style="font-size: x-small"><code>'
        needle += fr'{hash}</code>:</dt>\s+<dd>\s+'
        needle += r'<dl>\s+<dt><a href="'
        needle += fr'{archive_url}{pool_url}/{filename}'
        needle += r'"><code style="font-size: large"><strong>'
        needle += fr'{filename}</strong></code></a>'
        needle += fr'</dt>\s+<dd>\s+Seen in {archive} on {run} in\s+'
        needle += fr'<a href="{archive_url}{pool_url}/'
        needle += fr'">{pool_url}</a>.\s+<br />\s+Size: {size}\s+'
        needle += r'</dd>\s+</dl>\s+</dd>'

        return search(needle, haystack)
