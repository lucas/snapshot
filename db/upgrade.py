#!/usr/bin/python3

# Copyright (c) 2009 Peter Palfrader
# Copyright (c) 2020 Baptiste BEAUPLAT <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import os.path
import yaml
import psycopg2
sys.path.append(os.path.abspath(os.path.dirname(__file__))+'/../lib')
from dbhelper import DBHelper  # noqa: E402


def upgrade_one(db):
    upgrade_path = os.path.abspath(os.path.dirname(__file__))
    if upgrade_path not in sys.path:
        sys.path.append(upgrade_path)

    db.execute('BEGIN')
    r = db.query_one("SELECT value::INTEGER as db_revision "
                     "FROM config "
                     "WHERE name='db_revision'")
    rev = r['db_revision']
    print(f'Database is at {rev}')
    next = rev + 1

    upgrade_script = f'upgrade_{next}.py'
    if not os.path.exists(os.path.join(upgrade_path, upgrade_script)):
        print(f'Upgrade script {upgrade_script} does not exist. '
              'Maybe we are done.')
        return False

    upgrade = None
    try:
        module = __import__(upgrade_script.rstrip('.py'), globals(),
                            locals(), ['upgrade'], 0)
        upgrade = getattr(module, 'upgrade')
    except Exception as msg:
        sys.stderr.write(f'Cannot import upgrade_script {upgrade_script}: '
                         f'{msg}\n')
        sys.exit(1)
    upgrade(db)

    r = db.query_one("SELECT value::INTEGER as db_revision "
                     "FROM config "
                     "WHERE name='db_revision'")
    rev = r['db_revision']
    if rev != next:
        sys.stderr.write(f'Error: After running {upgrade_script} revision '
                         f'is not {next}\n')
        sys.exit(1)
    db.execute('COMMIT')
    print(f'Database upgrade to {rev} committed.')
    return True


def upgrade(db):
    try:
        while upgrade_one(db):
            pass

    except psycopg2.ProgrammingError as msg:
        db.execute('ROLLBACK')
        raise Exception('Unable to apply update', str(msg))


def usage(err=False):
    if err:
        f = sys.stderr
        exit = 1
    else:
        f = sys.stdout
        exit = 0
    f.write(f'Usage: {sys.argv[0]} <config.yaml>\n')
    sys.exit(exit)


def readConfig(conffile):
    return yaml.safe_load(open(conffile).read())


def main():
    if len(sys.argv) <= 1:
        usage(True)
    opt1 = sys.argv[1]
    del sys.argv[1]
    if opt1 == '-h' or opt1 == '--help':
        usage(False)

    config = readConfig(opt1)

    for kw in ('db',):
        if kw not in config:
            sys.stderr.write(f'Config file does not define {kw} keywords\n')
            sys.exit(1)
    if 'connectstring' not in config['db']:
        sys.stderr.write('Database config file does not define '
                         'db->connectstring keywords\n')
        sys.exit(1)

    db = DBHelper(config['db']['connectstring'])
    db.execute('ROLLBACK')

    upgrade(db)


if __name__ == '__main__':
    main()

# vim:set et:
# vim:set ts=4:
# vim:set shiftwidth=4
